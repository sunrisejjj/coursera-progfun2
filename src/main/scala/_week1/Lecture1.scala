/* Queries with For */
case class Book(title: String, authors: List[String])

val books = List(
    Book(title = "Effective Java",
        authors = List("Bloch, Joshua")),
    Book(title = "Effective Java 2",
        authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzlers",
        authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(title = "Programming in Scala",
        authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")),
    Book(title = "Война и Мир",
        authors = List("Толстой, Лев"))
)

println(for {
    b <- books
    a <- b.authors
    if a startsWith "V"
} yield b.title)

println(for {
    b <- books
    if b.title contains "Java"
} yield b.title)

// prints three times
println(for {
    b1 <- books
    b2 <- books
    if b1.title < b2.title
    a1 <- b1.authors
    a2 <- b2.authors
    if a1 == a2
} yield a1)

// fix 1: prints one time
println( { for {
    b1 <- books
    b2 <- books
    if b1.title < b2.title
    a1 <- b1.authors
    a2 <- b2.authors
    if a1 == a2
} yield a1 }.distinct)

// fix 2: switch books type from List[Book] to Set[Book]
