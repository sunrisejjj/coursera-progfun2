import java.util._

/* Functional Random Generators */
trait Generator[+T] {
    self => 

    def generate: T

    def map[S](f: T => S): Generator[S] = new Generator[S] {
        def generate = f(self.generate)
    }

    def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
        def generate = f(self.generate).generate
    }

}

// boilerplate version
val integers = new Generator[Int] {
    val rand = new java.util.Random
    def generate = rand.nextInt()
}

val booleans = new Generator[Boolean] {
    def generate = integers.generate > 0
}

val pairs = new Generator[(Int, Int)] {
    def generate = (integers.generate, integers.generate)
}

// testing boilerplate version
println(booleans.generate)
println(integers.generate)
println(pairs.generate)

// useful generators
def single[T](x: T): Generator[T] = new Generator[T] {
    def generate = x
}

def choose(lo: Int, hi: Int): Generator[Int] = 
    for (x <- integers) yield lo + x % (hi - lo)

def oneOf(xs: T*): Generator[T] = 
    for (idx <- choose(0, xs.length)) yield xs(idx)
