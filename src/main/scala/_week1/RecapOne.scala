// pattern matching etc
abstract class JSON {

    def show(): String = this match {
        case JSeq(elems) => "[" + (elems map(_.show) mkString ", ") + "]"
        case JObj(bindings) =>  {
            val members = bindings map {
                case (key, value) => "\"" + key + "\":" + value.show
            }
            "{" + (members mkString ", ") + "}"
        }
        case JNum(num) => num toString
        case JStr(str) => "\"" + str + "\""
        case JBool(bool) => bool toString
        case JNull() => "null"
    }

}

// case classes
case class JSeq(elems: List[JSON]) extends JSON {}
case class JObj(bindings: Map[String, JSON]) extends JSON {}
case class JNum(num: Double) extends JSON {}
case class JStr(str: String) extends JSON {}
case class JBool(b: Boolean) extends JSON {}
case class JNull() extends JSON {}

// testing
val data = JObj(Map(
    "firstName" -> JStr("John"),
    "lastName" -> JStr("Smith"),
    "address" -> JObj(Map(
        "streetAddress" -> JStr("21 2nd Street"),
        "streetAddress2" -> JNull(),
        "state" -> JStr("NY"),
        "postalCode" -> JNum(10021)
    )),
    "phoneNumbers" -> JSeq(List(
        JObj(Map(
            "type" -> JStr("home"),
            "number" -> JStr("123456789")
        )),
        JObj(Map(
            "type" -> JStr("fax"),
            "number" -> JStr("987654321")
        ))
    ))
))

println(data.show())


// for expression AND pattern matching
val filteredPersons = for {
    JObj(bindings) <- data
    JSeq(phones) = bindings("phoneNumbers")
    JObj(phone) <- phones
    JStr(digits) = phone("number")
    if digits startsWith "123"
}  yield (bindings("firstName"), bindings("lastName"))

/* Translation rules
 pat <- expr
  to
 x <- expr withFilter {
    case pat => true
    case _ => false
 } map {
    case pat => x
 }
 */


println(filteredPersons)


// partial functions
val f: PartialFunction[String, String] = { case "ping" => "pong" }
println(f.isDefinedAt("ping"))
println(f.isDefinedAt("abc"))
