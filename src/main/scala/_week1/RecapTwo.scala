// collection functions
abstract class List[+T] {

    // not so tail recursive implementations
    def map[U](f: T => U): List[U] = this match {
        case Nil => Nil
        case x :: xs => f(x) :: xs.map(f) // constructs result list element by element where X is an ELEMENT
    }

    def flatMap[U](f: T => List[U]): List[U] = this match {
        case Nil => Nil
        case x :: xs => f(x) ++ xs.flatMap(f) // constructs result list by concatenation of lists produced by f(x) where x is a COLLECTION
    }

    def filter(p: T => Boolean): List[T] = this match {
        case Nil => Nil
        case x :: xs =>
            if (p(x)) x :: xs.filter(p) else xs.filter(p)
    }
   
}

// for expressions
for {
    i <- 1 until n
    j <- 1 until i
    if (i + j % 2 == 0)
} yield (i, j)

/* Translation rules */
// 1. for (x <- e1) yield e2              <=>  e1.map(x => e2)
// 2. for (x <- e1 if f; s) yield e2      <=>  for (x <- e1.withFilter(x => f); s) yield e2  where withFilter is function which lazily filters iterable stuff
// 3. for (x <- e1; y <- e2; s) yield e3  <=>  e1.flatMap(x => for (y <- e2; s) yield e3)

